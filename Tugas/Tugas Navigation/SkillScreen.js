import React, { Component } from 'react';
import { FlatList, Text, StyleSheet, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import data from "../skillData.json"


const extractKey = ({ id }) => id

export default class SkillScreen extends React.Component {
    renderItem = ({ data }) => {
        return (
            <View style={styles.row}>
                <Icon style={styles.iconCategory} name={data.iconName} size={75}/>,
                <Icon style={styles.iconArrow} name={mdiChevronRight} size={23}/>,
                <Text style={styles.rowtxt1}>
                {data.skillName}
                </Text>,
                <Text style={styles.rowtxt2}>
                {data.categoryName}
                </Text>,
                <Text style={styles.rowtxt3}>
                {data.percentageProgress}
                </Text>
            </View>
        )
      }
    
      render() {
        return (
        <View style={styles.container}>
            <View style={styles.header}>
            <Image source={require('./')} />
            <Text style={styles.headerText}>SKILL</Text>
            </View>
          <FlatList
            style={styles.container}
            data={data}
            renderItem={this.renderItem}
            keyExtractor={extractKey}
          />
        </View>
        );
      }
    }
    
    const styles = StyleSheet.create({
      container: {
        flex: 1,
      },
      row: {
        padding: 15,
        marginBottom: 5,
        width: 343,
        height: 129,
        left: 14,
        backgroundColor: '#B4E9FF'
      },
      iconCategory: {
          position: 'absolute',
          left: 10,
          right: 10,
          top: 40,
          bottom: 40,
          color: '#003366'
      },
      iconArrow: {
          position: 'absolute',
          borderColor: '#003366',
          borderWidth: 8
      },
      rowtxt1: {
        position: 'absolute',
        fontSize: 24,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        width: 138,
        height: 28,
        left: 125,
        top: 214,
        color: '#003366',
      },
      rowtxt2: {
        position: 'absolute',
        fontSize: 16,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        width: 145,
        height: 19,
        left: 125,
        top: 247,
        color: '#3EC6FF',
      },
      rowtxt3: {
        position: 'absolute',
        fontSize: 48,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        fontWeight: 'bold',
        width: 91,
        height: 56,
        left: 194,
        top: 266,
        color: '#FFFFFF',
      }
      
    })