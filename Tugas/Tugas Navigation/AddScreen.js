import React from "react";
import { View, Text, StyleSheet } from "react-native";

// import { AuthContext } from "./context";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },

});

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

export const Add = () => (
  <ScreenContainer>
    <Text>Halaman Tambahan</Text>
  </ScreenContainer>
);
