import React from "react";
import { DrawerActions, NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack"
import{createBottomTabNavigator} from '@react-navigation/bottom-tabs'
import {Skill, Project, Add, Login, Details, About} from './Screens';
import {createDrawerNavigator} from '@react-navigation/drawer';


const AuthStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const LoginStack = createStackNavigator();
const About = createStackNavigator();


const LoginStackScreen = () => (
  <LoginStack.Navigator>
    <LoginStack.Screen name="Login" component={Login} />
    <LoginStack.Screen name="Details" component={Details} options={({route}) => ({
      title: route.params.name 
    })}/>
  </LoginStack.Navigator>
)

const AboutStackScreen = () => (
  <AboutStack.Navigator>
    <AboutStack.Screen name="About" component={About} />
  </AboutStack.Navigator>
)

const Skill = createStackNavigator();
const SkillStackScreen = () => (
  <SkillStack.Navigator>
    <SkillStack.Screen name="Skill" component={Skill} />
  </SkillStack.Navigator>
)

const Project = createStackNavigator();
const ProjectStackScreen = () => (
    <ProjectStack.Navigator>
      <ProjectStack.Screen name="Project" component={Project} />
    </ProjectStack.Navigator>
  )  

  const Add = createStackNavigator();
  const AddStackScreen = () => (
    <AddStack.Navigator>
      <AddStack.Screen name="Add" component={Add} />
    </AddStack.Navigator>
  )  

const TabScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Login" component={LoginStackScreen} />
      <Tabs.Screen name="Skill" component={SkillStackScreen} /> 
      <Tabs.Screen name="Project" component={ProjectStackScreen} /> 
      <Tabs.Screen name="Add" component={AddStackScreen} /> 
  </Tabs.Navigator>
);

export default () => (
  <NavigationContainer>
    <Drawer.Navigator>
      <Drawer.Screen name="Login" component={TabScreen} />
      <Drawer.Screen name="About" component={AboutStackScreen} />
    </Drawer.Navigator>
    {/*<AuthStack.Navigator>
      <AuthStack.Screen name="SignIn" component={SignIn} options={{ title: 'Sign In'}} />
      <AuthStack.Screen name="CreateAccount" component={CreateAccount} options={{title: 'Create Account'}}/>
    </AuthStack.Navigator>*/}
  </NavigationContainer>
)