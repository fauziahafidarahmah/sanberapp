import React from 'react';
import { Text, View, StyleSheet} from 'react-native';

const Register = ({
    params,
}) => {
    <View style={styles.home}>
        <Text>Welcome</Text>
        <Text>Sign up to continue</Text>
        <View style={[styles.layout, layoutStyle]}>
          <View style={styles.box} />
          <Text>Name</Text>
          <View style={styles.box} />
          <Text>Email</Text>
          <View style={styles.box} />
          <Text>Phone number</Text>
          <View style={styles.box} />
          <Text>Password</Text>
          <TouchableOpacity onPress={this.props.deleteMethod} style={styles.noteDelete}>
            <Text style={styles.noteDeleteText}>Sign up</Text>
        </TouchableOpacity>
      </View>
    </View>
};
const styles = StyleSheet.create({
    home : {
        flex: 1,
        justifyContent: 'center',
        alighItems: 'center',
    },
    noteDelete: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2980b9',
        padding: 10,
        top: 10,
        bottom: 10,
        right: 10
      },
      layout: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.05)',
      },
      box: {
        padding: 25,
        backgroundColor: 'steelblue',
        margin: 5,
      },
      noteDeleteText: {
        color: 'white',
      }
});
export default Register;