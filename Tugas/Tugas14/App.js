import React from 'react';
import Main from './components/Main';
import SkillScreen from './components/SkillScreen'
import data from './skillData.json';  

export default class App extends React.Component {
  render() {
    return (
      <SkillScreen screen={data.items[0]}/>
    )
  }
}